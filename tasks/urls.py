from django.urls import path
from tasks.views import create_task, tasks


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", tasks, name="show_my_tasks"),
]
